import mock from './mock-todo';
import * as Immutable from 'immutable';
import * as Cursor from 'immutable/contrib/cursor';
import * as Rx from 'rx';
import Todo from '../components/todolist/Todo';


const listOfTodos = [];

for(let i = 0; i < mock.length; i++){
    let TodoObj = new Todo(mock[i].title, mock[i].done);
    listOfTodos.push(TodoObj);
}

const initialState = {
    todos : listOfTodos,
    model: null,
    filter: '',
    pages: {}
};

class Context{

    constructor(){
        this.cursor = Cursor.from( Immutable.fromJS( initialState ), this.onContextChange.bind(this) );
        this.subject       = new Rx.BehaviorSubject(this.cursor);

    }

    onContextChange(newImmutable){
        this.cursor = Cursor.from( newImmutable, this.onContextChange.bind(this));
        this.subject.onNext(this.cursor);
    }


    subscribe(handleFunc){
        this.subject.subscribe(handleFunc);
    }

}

window.gloabl_context = new Context;

export default window.gloabl_context;