import * as React from 'react';
import  {Router, Route, browserHistory,hashHistory, IndexRoute} from 'react-router';
import * as ReactDOM from 'react-dom';
import App from './components/App';
import About from './components/About';
import Services from './components/Services';
import Contact from './components/Contact';
import TodoList from './components/todolist/TodoList';
import './components/css/General.css';

const Routes = () => {
    return (
        <Router history={hashHistory}>
            <Route path="/" component={App}>
                <IndexRoute  component={TodoList}/>
                <Route path="about" component={About}></Route>
                <Route path="services" component={Services}></Route>
                <Route path="contact" component={Contact}></Route>


            </Route>
        </Router>
    )

};


ReactDOM.render(<Routes/>, document.getElementById('root'));

