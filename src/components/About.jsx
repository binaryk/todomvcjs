import React, {
    Component,
} from 'react';


const About = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-3">
                    <p className="lead">Shop Name</p>
                    <div className="list-group">
                        <a href="#" className="list-group-item">All todos</a>
                        <a href="#" className="list-group-item">Done</a>
                        <a href="#" className="list-group-item">Removed</a>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default About;
