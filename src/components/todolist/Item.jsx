import React from 'react';


const Item = (props) => {
    const isDone   = props.node.done;
    const onDelete = props.onDelete;
    const onCheck  = props.onCheck;
    const onEdit   = props.onEdit;
    const title    = props.node.title;

    let toReturn = '';
    if(isDone){
        toReturn = (<li>
            { title }
            <button className="remove-item btn btn-default btn-xs pull-right">
                <span className="glyphicon glyphicon-remove"></span>
            </button>
        </li>)
    }else{
        toReturn = (<li className="ui-state-default">
            <div className="checkbox">
                <label htmlFor={props.index}>
                    <input id={props.index} type="checkbox" checked={ isDone } value="" onChange={onCheck} />

                    { title }

                </label>

                <button className="btn btn-xs btn-danger pull-right"
                        onClick={ onDelete }
                ><i className="fa fa-trash"></i>Delete</button>
                <button className="btn btn-xs btn-info pull-right"
                        onClick={ onEdit }
                ><i className="fa fa-trash"></i>Edit</button>
            </div>
        </li>)
    }

    return toReturn;

};

export default Item;