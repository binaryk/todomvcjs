import * as React from 'react';
import Context from '../../context/Context';
import * as Immutable from 'immutable';
import Accessors from '../../context/Accessors';
import TodoController from './Controller';


export default class Form extends React.Component{
    
    constructor(){
        super();
    }

    componentWillMount() {
        Context.subscribe(this.onContextUpdate.bind(this));
    }

    onContextUpdate(newGlobalCursor){
        this.setState({
            model : newGlobalCursor.get('model'),
            title : newGlobalCursor.get('model') && newGlobalCursor.get('model').title || ''
        });
    }

    updateModel(){
        this.setState({
            model: this.state.model,
            title: this.refs.title.value
        })
    }


    store(){

        const value = this.refs.title.value;

        if( Context.cursor.get('model') !== null){
            this.update()
        }else{
            TodoController.store(value);
        }
        this.refs.title.value = '';
    }

    update(){
        const index = Accessors.todos(Context.cursor).indexOf( Context.cursor.get('model') );
        TodoController.update(index, this.state.title)


    }

    search(){
        const filter = this.refs.search.value;
        Context.cursor.set('filter', filter);

    }
    
    render(){

        const value = this.state.title;
        const label = this.state.model ? "Update" : "Store";
        return(
            <div>
                <div className="row">
                    <div className="col-md-6">
                        
                        <input type="text" ref="title" className="form-control add-todo col-md-6"
                                                     placeholder="Add todo"

                                                     value={value}
                                                     onChange={this.updateModel.bind(this)}
                                                     onKeyDown={

                        function(event){
                            if(event.keyCode == 13){
                                this.store();
                            }
                        }.bind(this)

                       }/>
                    
                    
                    </div>
                    <div className="col-md-6">
                        <input type="text" ref="search" className="form-control add-todo col-md-6" placeholder="Search ..."
                            onChange={this.search.bind(this)}
                        />
                    </div>
                </div>
                <button id="store" className="btn btn-success margin-top" onClick={this.store.bind(this)}>
                    {label}
                </button>
            </div>
        )
    }
    
    
}