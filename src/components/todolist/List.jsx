import React from 'react';
import Context from '../../context/Context';
import Item from './Item'
import Accessors from '../../context/Accessors';
import TodoController from './Controller'
import * as Immutable from 'immutable';


class List extends React.Component {
    constructor(){
        super();
    }

    componentWillMount() {
        
        Context.subscribe(this.onContextChange.bind(this));

    }

    onContextChange(cursor){

        this.setState({
            todos: cursor.get('todos'),
            filter: cursor.get('filter')
        });

    }

    onDelete(element){

        const index = Accessors.todos(Context.cursor).indexOf( element );

        TodoController.delete(index);

    }

    onCheck(element){

        const index = Accessors.todos(Context.cursor).indexOf( element );
        console.log(Accessors.todos(Context.cursor).get(index));
        let test = Accessors.todos(Context.cursor).get(index).toggle();
        Accessors.todos(Context.cursor).set(index, Immutable.fromJS(test) );

    }

    onEdit(element){

        const index = Accessors.todos(Context.cursor).indexOf( element );

        const cursorToElement = Accessors.todos(Context.cursor).get(index);

        Context.cursor.set('model', cursorToElement);


    }



    render() {

        const items = this.state.todos.
        filter(
            (element) => {
                return element.done == false && element.title.indexOf(this.state.filter) > -1
            }
        ).map( (element, index) => {
            return (
                <Item
                    node={element}
                    key={index}
                    onDelete={this.onDelete.bind(this, element)}
                    onCheck={this.onCheck.bind(this, element)}
                    onEdit={this.onEdit.bind(this, element)}
                    index={index}
                />
            )
        });


        return (
            <ul id="sortable" className="list-unstyled">

                {items}

            </ul>

        );
    }
}

export default List;
