import React from 'react';
import Context from '../../context/Context';
import Item from './Item';


class DoneTodo extends React.Component {
    constructor(){
        super();
    }

    componentWillMount() {
        Context.subscribe(this.onContextUpdate.bind(this));
    }

    onContextUpdate(newGlobalCursor){
        this.setState(
            {
                todos: newGlobalCursor.get('todos')
            }
        )
    }


    render() {

        const items = this.state.todos
            .filter( (element) => {
                return element.done === true
            })
            .map( (element, index) => {
            return <Item
                node={element}
                key={index}
            />
        });

        return (
            <div className="col-md-6">
                <div className="todolist">
                    <h1>Already Done</h1>
                    <ul id="done-items" className="list-unstyled">

                        {items}


                    </ul>
                </div>
            </div>
        );
    }
}

export default DoneTodo;
