import React from 'react';
import './List.css';
import List from './List';
import Form from './Form';
import DoneTodo from './DoneTodo';


export default class TodoList  extends  React.Component{
    constructor(){
        super();
    }


    render(){

        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="todolist not-done">
                            <h1>Todos 9899</h1>

                                <Form/>
                                <hr/>
                                <List/>

                                <div className="todo-footer">
                                    <strong><span className="count-todos"></span></strong> Items Left
                                </div>
                        </div>
                    </div>

                                <DoneTodo/>


                </div>
                </div>

        )
    }
}