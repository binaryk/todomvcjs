export default class Todo {

    constructor(title, done){
        this.title = title;
        this.done  = done;
    }



    toggle(){
        this.done = !this.done;
        return this;
    }

    changeText(text){
        this.title = text;
        return this;
    }

}