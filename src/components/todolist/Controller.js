import Context from '../../context/Context';
import * as Immutable from 'immutable';
import Accessors from '../../context/Accessors';
import Todo from './Todo';


export default new class TodoController {


    update(index, title){
        Accessors.todos(Context.cursor).update( index,  oldTodoInstance => {
            oldTodoInstance.title = title;
            return oldTodoInstance;
        });

        Context.cursor.set('model', null);
    }
    
    delete(index){
        Accessors.todos(Context.cursor).update( oldList => {

            return oldList.splice(index, 1);

        } );
    }

    store(value){

       /* const Node = {
            title: value,
            done: false
        };*/

        const Node = new Todo(value, false);
        Context.cursor.get('todos').update( (oldList) => {

            return oldList.push( Immutable.fromJS(Node) );

        });

    }




}