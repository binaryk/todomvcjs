import React, {
    Component,
    PropTypes,
} from 'react';
import Header from './Header';


class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="container offset">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

App.propTypes = {};
App.defaultProps = {};

export default App;
